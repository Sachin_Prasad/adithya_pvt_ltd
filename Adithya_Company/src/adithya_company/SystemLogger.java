/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adithya_company;

import java.io.File;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;
import org.apache.log4j.RollingFileAppender;

/**
 *
 * @author Kushan
 */
public class SystemLogger {
    public static void initLogger(){
        try {
            String path = "D:\\ADITHAYA SHOE\\LOGGER\\Logs.log";
            File file = new File("D:\\ADITHAYA SHOE\\LOGGER");
            if (!file.exists()) {
                file.mkdirs();
            }
            PatternLayout  patternLayout = new PatternLayout("%-5p %d(yyyy-MMM-dd HH:mm:ss) %c %m %n");
            RollingFileAppender appender = new RollingFileAppender(patternLayout,path);
            appender.setMaxFileSize("50MB");
            appender.setName("AdithyaCompany");
            appender.activateOptions();
            Logger.getRootLogger().addAppender(appender);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public static void main(String[] args) {
        initLogger();
        Logger log =Logger.getLogger("AdithyaCompany");
        log.info("This is information");
        log.warn("this is a warning!");
        log.error("this is error");
        
    }
}