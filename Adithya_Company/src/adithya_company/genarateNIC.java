/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adithya_company;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Kushan
 */
public class genarateNIC {

    static String id;
    static int data;
    static String month;
    static int date;
    static String year;

    public static void display(String ID) {
        id = ID;
        if (getSex() == "Male") {
            employeeManagement.rad_male.setSelected(true);
        }
        if (getSex() == "Female") {
            employeeManagement.rad_female.setSelected(true);
        }
        try {
            String dob = getYear() + "-" + getMonth() + "-" + getDate();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date parse = sdf.parse(dob);
            employeeManagement.emp_dob.setDate(parse);

            try {
                Date d2 = sdf.parse(dob);
                String today = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
                Date d1 = sdf.parse(today);
                long time1 = sdf.parse(sdf.format(d1)).getTime();
                long time2 = sdf.parse(sdf.format(d2)).getTime();
                long diff = time1 - time2;
                long datediff = diff / (1000 * 60 * 60 * 24);
                int year = (int) (datediff / 365);
                employeeManagement.txt_age.setText("" + year);
            } catch (Exception e) {
                
            }

        } catch (Exception e) {
            
        }
    }

    private static int getData() {
        if (id.length() == 10) {
            data = Integer.parseInt(id.substring(2, 5));
        } else {
            data = Integer.parseInt(id.substring(4, 7));
        }
        return data;
    }

    private static int data() {
        int data = getData() % 500;
        return data;
    }

    private static String getMonth() {

        if (data() <= 31) {
            month = "01";
        } else if (data() <= 60) {
            month = "02";
        } else if (data() <= 91) {
            month = "03";
        } else if (data() <= 121) {
            month = "04";
        } else if (data() <= 152) {
            month = "05";
        } else if (data() <= 182) {
            month = "06";
        } else if (data() <= 213) {
            month = "07";
        } else if (data() <= 244) {
            month = "08";
        } else if (data() <= 274) {
            month = "09";
        } else if (data() <= 305) {
            month = "10";
        } else if (data() <= 335) {
            month = "11";
        } else {
            month = "12";
        }
        return month;
    }

    private static int getDate() {

        if (data() <= 31) {
            date = data();
        } else if (data() <= 60) {
            date = (data() - 31);
        } else if (data() <= 91) {
            date = (data() - 60);
        } else if (data() <= 121) {
            date = (data() - 91);
        } else if (data() <= 152) {
            date = (data() - 121);
        } else if (data() <= 182) {
            date = (data() - 152);
        } else if (data() <= 213) {
            date = (data() - 182);
        } else if (data() <= 244) {
            date = (data() - 213);
        } else if (data() <= 274) {
            date = (data() - 244);
        } else if (data() <= 305) {
            date = (data() - 274);
        } else if (data() <= 335) {
            date = (data() - 305);
        } else {
            date = (data() - 335);
        }
        return date;
    }

    private static String getYear() {

        if (id.length() == 10) {
            year = "19" + id.substring(0, 2);
        } else {
            year = id.substring(0, 4);
        }
        return year;
    }

    private static String getSex() {
        String sex;
        if (getData() < 500) {
            sex = "Male";
        } else {
            sex = "Female";
        }
        return sex;
    }
    
}
